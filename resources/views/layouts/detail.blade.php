<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8" />
  <title>IuranApps</title>
  {{-- <meta name="description" content="" /> --}}
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
  <link rel="shortcut icon" href="/images/Resources/harbolnas_logo.png">
  <link rel="stylesheet" href="/plugins/template/animate.css/animate.css" type="text/css" />
  <link rel="stylesheet" href="/plugins/template/font-awesome/css/font-awesome.css" type="text/css" />
  <link rel="stylesheet" href="/plugins/template/waves/dist/waves.css" type="text/css" />

  <link rel="stylesheet" href="/plugins/template/bootstrap/dist/css/bootstrap.css" type="text/css" />
  <link rel="stylesheet" href="/plugins/template/styles/font.css" type="text/css" />
  <link rel="stylesheet" href="/plugins/template/styles/app.css" type="text/css" />

  <link rel="stylesheet" href="/css/base.css">
  @yield('styles')
  @yield('modal')
</head>
<body>
  <div class="app">

    <!-- content -->
    <div id="content" class="app-content" role="main">
      <div class="box">
        <!-- Content Navbar -->
        <div class="navbar md-whiteframe-z1 no-radius bg-white">
          <div class="text-center m-sm">
            <a href="/" class="fa fa-arrow-left pull-left m-sm" style="font-size: 20px"></a>
            <img src="/iuran-logo.png" style="width:140px; margin-left: -32px">
          </div>
        </div>

        <!-- Content -->
        <div class="box-row">
          <div class="box-cell">
            <div class="box-inner" style="max-width: 504px; margin: auto;">
              @yield('contents')
            </div>
          </div>
        </div>
        <!-- / content -->

      </div>
    </div>
  </div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
{{-- <script src="/plugins/template/jquery/dist/jquery.js"></script> --}}
<script src="/js/base.js"></script>
<script src="/plugins/template/bootstrap/dist/js/bootstrap.js"></script>
<script src="/plugins/template/waves/dist/waves.js"></script>
@yield('scripts')
</body>
</html>
