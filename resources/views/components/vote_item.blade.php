{{-- card --}}
<div class="m-l-md card vote-item">
  <div class="card-body">
    {{-- radio --}}
    <div class="form-check text-right">
      <input class="form-check-input position-static" type="radio" name="voteitems" id="vote_item-{{$id}}" value="{{$id}}">
    </div>
    {{-- image --}}
    <div onclick="vote({{$id}})" class="vote-img" style="background-image: url('{{$image}}')">
      <div style="width:150px; height:150px;"></div>
    </div>
    <div class="m-t-sm font-bold">
      <a href="{{$link}}" target="_blank">{{$name}}</a>
    </div>
    <div class="">
      Rp {{number_format($price,0,',','.')}}
    </div>
    <small class="">
      Vote: {{$count}}
    </small>
  </div>
</div>
