@extends('layouts.detail')

@section('styles')
  <style>

  </style>
@endsection

@section('contents')
  <form class="p-lg">
    <div class="form-group">
      <label for="name">Nama</label>
      <input type="text" class="form-control" id="name" name="name">
    </div>
    <div class="form-group">
      <label for="budget">Rencana Iuran</label>
      <input type="number" class="form-control" id="budget" name="budget">
      <p><small class="text-muted">*note: rencana iuran adalah jumlah yang akan kamu transfer</small></p>
    </div>
    <div class="text-center m-t-md">
      <button class="btn btn-info w-sm" type="button" name="button">Simpan</button>
    </div>
  </form>
@endsection

@section('scripts')
@endsection
