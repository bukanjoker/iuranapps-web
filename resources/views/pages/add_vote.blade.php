@extends('layouts.detail')

@section('styles')
  <style>

  </style>
@endsection

@section('contents')
  <form class="p-lg">
    <div class="form-group">
      <label for="name">Nama Item</label>
      <input type="text" class="form-control" id="name" name="name">
    </div>
    <div class="form-group">
      <label for="price">Harga</label>
      <input type="text" class="form-control" id="price" name="price">
    </div>
    <div class="form-group">
      <label for="image">ImageUrl</label>
      <input type="text" class="form-control" id="image" name="image" placeholder="http://">
    </div>
    <div class="form-group">
      <label for="link">Link</label>
      <input type="text" class="form-control" id="link" name="link" placeholder="http://" required>
      <p><small class="text-muted">*note: gunakan Link olshop/e-commerce</small></p>
    </div>
    <div class="text-center m-t-md">
      <button class="btn btn-info w-sm" type="button" name="button">Tambah</button>
    </div>
  </form>
@endsection

@section('scripts')
@endsection
