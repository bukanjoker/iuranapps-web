@extends('layouts.satu')

@section('styles')
  <style>
    .content-head {
      padding-bottom: 72px !important;
      margin-top: -8px;
      margin-left: -15px;
      margin-right: -15px;
    }
    .vote-item {
      width: 200px;
      display: inline-block;
      margin-right: -12px;
    }
    .vote-items {
      margin-top: -20px;
      overflow-x: scroll;
      white-space: nowrap;
    }
    .vote-img {
      background-repeat: no-repeat;
      background-size: cover;
    }
    div::-webkit-scrollbar{
      display: none;
    }

    @media (max-width: 480px) {
      .col-xs-9 > .h2 {
        font-size: 20px;
      }
    }
    @media (min-width: 480px) {
      .col-xs-3 > .btn {
        width: 100%;
      }
    }
  </style>
@endsection

@section('contents')
  <div class="p-xl content-head bg-info">
    <div class="h3 text-center">
      Iuran Kado Abid
    </div>
    <div class="m-t-sm text-justify">
      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris suscipit placerat dui, eu lacinia urna mattis eu. Quisque vitae lobortis ante. Proin feugiat hendrerit tellus vitae mattis. Nullam a mi purus. Praesent quis lacinia ex
    </div>
  </div>
  <div class="m-md card" style="margin-top: -42px">
    <div class="card-body">
      <div class="row">
        <div class="col-xs-9">
          <small class="text-muted">
            Rencana Iuran yang sudah tercatat:
          </small>
          <div class="h2 font-bold text-success">
            Rp {{number_format($budget,0,',','.')}}
          </div>
        </div>
        <div class="col-xs-3 m-t-lg">
          <a href="/iuran" class="btn btn-success">JOIN</a>
        </div>
      </div>
      <div class="m-t-sm text-info">
        @if ($myiuran['budget'] != 0)
          <div class="">
            Iuran saya: Rp {{number_format($myiuran['budget'],0,',','.')}}
          </div>
        @endif
        @if ($myiuran['vote'] != '')
          <div class="">
            Vote saya: {{$myiuran['vote_name']}}
          </div>
        @endif
      </div>
    </div>
  </div>
  <div class="p-lg font-bold text-muted" style="margin-top: -20px">
    Voting Item
  </div>
  <div class="vote-items">
    @foreach ($vote_items as $i => $item)
      @include('components.vote_item',[
        'id'=>$item['id'],
        'name'=>$item['name'],
        'price'=>$item['price'],
        'image'=>$item['image'],
        'count'=>$item['count'],
        'link'=>$item['link']
      ])
    @endforeach
  </div>
  <div class="text-center m-b-lg">
    @if (count($vote_items) != 0)
      <button class="btn btn-success w-sm">Vote</button>
    @endif
    <a href="/vote-add" class="btn btn-info m-l-sm w-sm">Tambah</a>
  </div>

@endsection

@section('scripts')
  <script>
    $(document).ready(function(){
      vote({{$myiuran['vote']}})
    })

    function vote(id) {
      $('#vote_item-'+id).prop('checked', true);
    }
  </script>
@endsection
