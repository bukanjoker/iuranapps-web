<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function frontpage(Request $request)
    {
      $budget = 0;
      $myvote = '';

      $vote_items = [
        [
          'id'=>1,
          'name'=>'Item 1',
          'price'=>'200000',
          'image'=>'https://ecs7.tokopedia.net/img/cache/200-square/product-1/2018/7/8/8859714/8859714_1c63d5ba-012a-4445-bc92-1c4c02c5038f_700_461.jpg',
          'count'=>3,
          'link'=>'https://tokopedia.link/pMbw2JH45Y'
        ],
        [
          'id'=>2,
          'name'=>'Item 2',
          'price'=>'300000',
          'image'=>'https://bellard.org/bpg/2.png',
          'count'=>3,
          'link'=>'#'
        ],
        [
          'id'=>3,
          'name'=>'Item 3',
          'price'=>'250000',
          'image'=>'https://bellard.org/bpg/2.png',
          'count'=>3,
          'link'=>'#'
        ],
        [
          'id'=>4,
          'name'=>'Item 4',
          'price'=>'230000',
          'image'=>'https://bellard.org/bpg/2.png',
          'count'=>3,
          'link'=>'#'
        ]
      ];

      $myiuran['budget'] = 300000;
      $myiuran['vote'] = 1;
      $myiuran['vote_name'] = 'Item 1';

      return view('pages.frontpage',compact('budget','myiuran','vote_items'));
    }

    public function addVote()
    {
      return view('pages.add_vote');
    }

    public function iuran($value='')
    {
      return view('pages.iuran');
    }
}
